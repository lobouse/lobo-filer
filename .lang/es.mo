��    2      �  C   <      H     I     O  /   `     �     �     �     �     �  	   �     �     �     �     �  	   �               1  (   7     `     u     }     �  
   �     �     �     �     �     �     �     �  
   �     �     �     �  (     
   *     5  3   A     u     |     �  .   �     �     �     �     �     �     �  =      �   >  	        &  4   :     o     |     �     �     �     �     �     �     �     �     �      	     	     1	  .   D	  '   s	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     4
     H
     P
     V
  *   [
  
   �
     �
  :   �
     �
     �
  
   �
  -        5  
   :     E     M     h  	   {  B   �                +   $      .                 /      2            1       #   "                     *                        '                    	   )                            %       !          ,           
         &      0      -   (    About About lobo-filer An archive manager utility for the LoboDesktop. Build Cancel Check Update Checking version Close Component Copyright © 2020-2020 Country Create Create file Developed Download Canceled Downloading new version Email Empty path, you must select directory... Error DB connection. Extract Extract file Extract in: File Name: Language License Mail Name New No files to compress No files to extract No updates Ok Open Path Please select a directory to compress... Processing Select File Some error occurred. Failed to create archive file. Thanks The file already exists Theme There is a new version, do you want to update? Type Update Version Version App Version Gambas Website lobo-filer is an archive manager utility for the LoboDesktop. Project-Id-Version: lobo-filer 3.15.2
PO-Revision-Date: 2020-11-26 17:34 UTC
Last-Translator: herberth <herberth@dev>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Acerca de Acerca de lobofiler Una utilidad de gestor de archivos para LoboDesktop. Compilación Cancelar Verificando Actualizaciones Verificando version Cerrar Componenete Derechos de Autor Pais Crear Crear archivo Desarrollado Descarga cancelada Descargando la nueva versión Correo Electronico Ruta vacía, debe seleccionar el directorio... Error en la conexión de base de datos. Extraer Extraer archivo Extraer en: Nombre del archivo Lenguaje Licencia Correo Nombre Nuevo No hay archivos para comprimir No hay archivos para extraer Sin actualizaciones Aceptar Abrir Ruta Por favor selecione directorio a comprimir Procesando Seleccione Archivo Se ha producido algún error. No se pudo crear el archivo. Gracias El archivo ya existe Apariencia Hay una nueva versión, ¿quieres actualizar? Tipo Actualizar Version Versión de la Aplicación Versión de Gambas Sitio Web lobo-filer es una utilidad de gestor de archivos para LoboDesktop. 