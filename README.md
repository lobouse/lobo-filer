
# lobo-filer

_lobo-filer_ An archive manager utility for the _lobodesktop.

Lobo Filer is only a front-end (a graphical interface) to archiving programs
like tar and zip. The supported file types are:

7-Zip Compressed File (.7z)
WinRAR Compressed Archive (.rar)
uncompressed (.tar)
or compressed with:

gzip (.tar.gz , .tgz)
bzip (.tar.bz , .tbz)
bzip2 (.tar.bz2 , .tbz2)
xz (.tar.xz)

ZIP Archive (.zip)

<div align="center"> 
    <img src="img/screenshots/Screen_1.png" width="400px"</img> 
</div> 